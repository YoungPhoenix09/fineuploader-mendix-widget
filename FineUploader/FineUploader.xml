<?xml version="1.0" encoding="utf-8" ?>
<widget id="FineUploader.widget.FineUploader" needsEntityContext="true" xmlns="http://www.mendix.com/widget/1.0/">
    <name>Fine Uploader</name>
    <description>Mendix widget implementation of the Fine Uploader file drop library.</description>

    <icon />

    <properties>
        <property key="fileDocEntity" type="entity" required="true">
            <caption>File Document Entity</caption>
            <category>Settings</category>
            <description>Entity the should be used for storing uploads.  This should be a specialization of the System.FileDocument entity.</description>
        </property>
        <property key="assocContext" entityProperty="fileDocEntity" type="entity" isPath="yes" pathType = "reference" required="false">
            <caption>Context Association</caption>
            <category>Settings</category>
            <description>Path to associate entiy to context.</description>
        </property>
        <property key="idAttr" type="attribute" entityProperty="fileDocEntity" required="true">
            <caption>File ID attribute</caption>
            <category>Settings</category>
            <description>Autonumber attribute used for identifying the upload</description>
            <attributeTypes>
                <attributeType name="AutoNumber" />
            </attributeTypes>
        </property>
        <property key="fileSizeAttr" type="attribute" entityProperty="fileDocEntity" required="false">
            <caption>File Size attribute</caption>
            <category>Settings</category>
            <description>Optional integer attribute to store file size.</description>
            <attributeTypes>
                <attributeType name="Integer" />
            </attributeTypes>
        </property>
        <property key="concurrency" type="integer" defaultValue="3">
            <caption>File Concurrency</caption>
            <category>Settings</category>
            <description>Limit on how uploads send at once</description>
        </property>
        <property key="chunking" type="boolean" defaultValue="false">
            <caption>Use File Chunking</caption>
            <category>Settings</category>
            <description>Send files as split partitions</description>
        </property>
        <property key="concurrentChunks" type="boolean" defaultValue="false">
            <caption>Concurrent Chunks</caption>
            <category>Settings</category>
            <description>Send multiple chunks of a file at one time.  Number of chunks sent depends on the File Concurrency when this option is enabled.</description>
        </property>
        <property key="maxChunkSize" type="integer" defaultValue="2000000">
            <caption>Max Chunk Size</caption>
            <category>Settings</category>
            <description>Max size of each chunk partition of a file</description>
        </property>
        
        <property key="keepAlive" type="boolean" defaultValue="false">
            <caption>Keep Session Alive</caption>
            <category>Settings</category>
            <description>Creates click events every time progress of file (or chunk, if enabled) upload changes.</description>
        </property>
        <property key="maxNoFiles" type="integer" defaultValue="0">
            <caption>Max Number of Files</caption>
            <category>Validation</category>
            <description>Limit on how many files can be dropped (0 = unlimited)</description>
        </property>
        <property key="maxFileSize" type="integer" defaultValue="0">
            <caption>Max File Size</caption>
            <category>Validation</category>
            <description>Maximum file size in MB</description>
        </property>
        <property key="fileTypeList" type="object" isList="true" required="false">
            <caption>Valid File Types</caption>
            <category>Validation</category>
            <description>List of acceptable file extensions and/or MIME types</description>
            <properties>
                <property key="type" type="string" required="true">
                    <caption>File/MIME type</caption>
                    <category>Type</category>
                    <description>Name of the file/MIME type</description>
                </property>
            </properties>
        </property>
        <property key="fileSzErrMsg" type="string" required="false">
            <caption>File Size Error Msg</caption>
            <category>Validation</category>
            <description>Message to be displayed when the size of a file is too big</description>
        </property>
        <property key="fileTypeErrMsg" type="string" required="false">
            <caption>File Type Error Msg</caption>
            <category>Validation</category>
            <description>Message to be displayed when a file has an invalid extension or MIME type</description>
        </property>
        <property key="fileDupErrMsg" type="string" required="false">
            <caption>Duplicate File Error Msg</caption>
            <category>Validation</category>
            <description>Message to be displayed when a file with the same name as another is attempted to be added.</description>
        </property>
        <property key="errMethod" type="enumeration" defaultValue="dialog">
            <caption>Erroring Method</caption>
            <category>Validation</category>
            <description>Determines whether files rejected by validation will cause a dialog to appear or whether the error will show inline with the represented file element</description>
            <enumerationValues>
                <enumerationValue key="dialog">Dialog</enumerationValue>
                <enumerationValue key="inline">Inline</enumerationValue>
            </enumerationValues>
        </property>
        <property key="validationMsg" type="boolean" defaultValue="false">
            <caption>Show Validation Text</caption>
            <category>Validation</category>
            <description>Show message that displays above drop area when invalid files are placed</description>
        </property>
        <property key="vMsgText" type="string" required="false">
            <caption>Validation Text</caption>
            <category>Validation</category>
            <description>Message to be displayed above drop area</description>
        </property>
        <property key="autoLoad" type="boolean" defaultValue="true">
            <caption>Auto Upload Files</caption>
            <category>Settings</category>
            <description>Automatically upload files that are dropped</description>
        </property>
        <property key="uploadBtnMf" type="microflow" required="false">
            <caption>Upload Button Microflow</caption>
            <category>Settings</category>
            <description>Microflow to run when upload button is clicked.  Microflow must be able to accept context entity as a parameter and return a boolean as a success value.</description>
            <returnType type="Boolean" />
        </property>
        <property key="allowDups" type="boolean" defaultValue="true">
            <caption>Allow Duplicate</caption>
            <category>Settings</category>
            <description>Should duplicate file names be allowed for upload</description>
        </property>
        <property key="width" type="integer" defaultValue="500">
            <caption>Width</caption>
            <category>Appearance</category>
            <description>Width of the upload window in pixels</description>
        </property>
        <property key="height" type="integer" defaultValue="315">
            <caption>Height</caption>
            <category>Appearance</category>
            <description>Height of the upload window in pixels.  Minimum is 315px.</description>
        </property>
        <property key="btnCaption" type="string" defaultValue="Start Upload">
            <caption>Button Caption</caption>
            <category>Appearance</category>
            <description>Caption of the upload button</description>
        </property>
        <property key="btnImage" type="image" required="false">
            <caption>Button Image</caption>
            <category>Appearance</category>
            <description>Image to be display next to the caption of the upload button</description>
        </property>
        <property key="hideBtn" type="boolean" defaultValue="false">
            <caption>Hide Button After Click</caption>
            <category>Appearance</category>
            <description>Option to hide upload button after it is clicked</description>
        </property>
        <property key="showClearBtn" type="boolean" defaultValue="true">
            <caption>Show Clear Button</caption>
            <category>Appearance</category>
            <description>Show button for clearing uploads</description>
        </property>
        <property key="clearBtnMf" type="microflow" required="false">
            <caption>Clear Button Microflow</caption>
            <category>Settings</category>
            <description>Microflow to run when clear button is clicked.  Microflow must be able to accept context entity as a parameter and return a boolean as a success value.</description>
            <returnType type="Boolean" />
        </property>
        <property key="dropCapOrImg" type="enumeration" defaultValue="dropCap">
            <caption>Drop Caption Or Image</caption>
            <category>Appearance</category>
            <description></description>
            <enumerationValues>
                <enumerationValue key="dropCap">Caption</enumerationValue>
                <enumerationValue key="dropImg">Image</enumerationValue>
            </enumerationValues>
        </property>
        <property key="dropCaption" type="string" defaultValue="Drop files here">
            <caption>Drop Area Caption</caption>
            <category>Appearance</category>
            <description>Caption of the file drop area</description>
        </property>
        <property key="dropImage" type="image" required="false">
            <caption>Drop Area Image</caption>
            <category>Appearance</category>
            <description>Image to be placed in the file drop area</description>
        </property>
        <property key="clickZn" type="boolean" defaultValue="false">
            <caption>Click To Upload</caption>
            <category>Appearance</category>
            <description>Hides the button to select files, and makes it so files can be added by clicking the drop area.</description>
        </property>
        <property key="overallProgress" type="boolean" defaultValue="true">
            <caption>Show Overall Progress</caption>
            <category>Appearance</category>
            <description>Show the overall progress bar at the top of the file drop area</description>
        </property>
        <property key="showFileSize" type="boolean" defaultValue="false">
            <caption>On Submit File Size</caption>
            <category>Appearance</category>
            <description>Make file size visible when the file has been submitted prior to upload.</description>
        </property>
        <property key="debugMsgs" type="boolean" defaultValue="false">
            <caption>Show Debug Messages</caption>
            <category>Debug</category>
            <description>Console log messages are printed.</description>
        </property>
        <property key="allowDelete" type="boolean" defaultValue="true">
            <caption>Allow File Delete</caption>
            <category>File Controls</category>
            <description>Shows Delete button for uploads.</description>
        </property>
        <property key="allowNameEdit" type="boolean" defaultValue="true">
            <caption>Allow File Name Edit</caption>
            <category>File Controls</category>
            <description>Shows Edit button for uploads.</description>
        </property>
        <property key="allowCancel" type="boolean" defaultValue="true">
            <caption>Allow File Cancel</caption>
            <category>File Controls</category>
            <description>Shows Cancel button for uploads.</description>
        </property>
        <property key="allowRetry" type="boolean" defaultValue="true">
            <caption>Allow File Retry</caption>
            <category>File Controls</category>
            <description>Shows Retry button for uploads.</description>
        </property>
        <property key="allCompleteMf" type="microflow" required="false">
            <caption>On All Complete</caption>
            <category>Events</category>
            <description>The microflow to execute when all files finish uploading.  Microflow must be able to accept context entity as a parameter.</description>
            <returnType type="Void" />
        </property>
        <property key="onErrMf" type="microflow" required="false" entityProperty="fileDocEntity">
            <caption>On Error</caption>
            <category>Events</category>
            <description>The microflow to execute when an error occurs.  Microflow must be able to accept the type of entity selected for the File Document Entity.</description>
            <returnType type="Void" />
        </property>
        <property key="onErrAttr" type="attribute" entityProperty="fileDocEntity" required="false">
            <caption>On Error Attribute</caption>
            <category>Events</category>
            <description>Optional string attribute to store error message returned by the On Error event.</description>
            <attributeTypes>
                <attributeType name="String" />
            </attributeTypes>
        </property>
    </properties>
</widget>

