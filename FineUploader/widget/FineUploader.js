/*jslint white:true, nomen: true, plusplus: true */
/*global $:true, window, mx, define, require, logger, dojo, mxui, console, document, MouseEvent*/
/*
    Template
    ========================

    @file      : Template.js
    @version   : 1.0.0
    @author    : Jonathan Payne
    @date      : Thu, 25 Feb 2016 15:22:00 GMT
    @copyright : 2016
    @license   :

    Documentation
    ========================
    Describe your widget here.
*/

// Required module list. Remove unnecessary modules, you can always get them back from the boilerplate.
require([
    "mxui/widget/_WidgetBase",
    "dijit/_TemplatedMixin",

    "mxui/dom",
    "dojo/dom",
    "dojo/dom-prop",
    "dojo/dom-geometry",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/text",
    "dojo/html",
    "dojo/_base/event",
    "dojo/has",

    //"FineUploader/lib/jquery-1.11.2",
    "dojo/text!FineUploader/widget/template/FineUploader.html",
    "FineUploader/lib/fineUploader/fine-uploader"
], function(_WidgetBase, _TemplatedMixin, dom, dojoDom, dojoProp, dojoGeometry, dojoClass, dojoStyle, dojoConstruct, dojoArray, dojoLang, dojoText, dojoHtml, dojoEvent, has, /*$,*/ widgetTemplate, qq) {
    "use strict";

    var widget;
    //$.noConflict();

    //Add test to determine if jquery is already in the project
    has.add("jquery", function () {
        var found = false;

        if (!!$ && !!$.fn && !!$.fn.jquery) {
            found = true;
        }

        return found;
    });

    //Dynamically require jquery
    if (!has("jquery")) {
        require(["FineUploader/lib/jquery-1.11.2"], function (_jQuery) {
            $ = _jQuery.noConflict(true);
        });
    }


    //Setup wdiget object
    widget = {
        // Add Mixins
        mixins: [ _WidgetBase, _TemplatedMixin ],

        // _TemplatedMixin will create our dom node using this HTML template.
        templateString: widgetTemplate,

        // DOM elements
        inputNodes: null,
        colorSelectNode: null,
        colorInputNode: null,
        infoTextNode: null,

        // Parameters configured in the Modeler.
        inputargs: {
            fileDocEntity: "",
            assocContext: '',
            idAttr: -1,
            fileSizeAttr: "",
            concurrency: 3,
            chunking: false,
            concurrentChunks: false,
            maxChunkSize: 2000000,
            maxNoFiles: 0,
            maxFileSize: 0,
            fileTypeList: null,
            fileSzErrMsg: "",
            fileTypeErrMsg: "",
            fileDupErrMsg: "",
            errMethod: "dialog",
            validationMsg: false,
            vMsgText: "",
            autoLoad: true,
            uploadBtnMf: "",
            allowDups: true,
            keepAlive: false,
            width: 500,
            height: 315,
            btnCaption: "Start Upload",
            btnImage: "",
            hideBtn: false,
            showClearBtn: true,
            clearBtnMf: "",
            dropCapOrImg: "dropCap",
            dropCaption: "Drop files here",
            dropImage: "",
            clickZn: false,
            overallProgress: true,
            showFileSize: false,
            debugMsgs: false,
            allCompleteMf: "",
            onErrMf: "",
            onErrAttr: ""
        },

        // Internal variables. Non-primitives created in the prototype are shared between all widget instances.
        _handles: null,
        _contextObj: null,
        _alertDiv: null,
        invalidFiles: null,

        // dojo.declare.constructor is called to construct the widget instance. Implement to initialize non-primitive properties.
        constructor: function() {
            // Uncomment the following line to enable debug messages
            //logger.level(logger.DEBUG);
            logger.debug(this.id + ".constructor");
            this.invalidFiles = {};
        },

        // dijit._WidgetBase.postCreate is called after constructing the widget. Implement to do extra setup work.
        postCreate: function() {
            logger.debug(this.id + ".postCreate");
            this.domNode.style.width = this.width + "px";
            this.domNode.style.height = this.height + "px";
            this.domNode.style.minHeight = "315px";
            this.setupStyles();
            this.alterTemplate();
        },

        // mxui.widget._WidgetBase.update is called when context is changed or initialized. Implement to re-render and / or fetch data.
        update: function(obj, callback) {
            logger.debug(this.id + ".update");

            this._contextObj = obj;
            this.initFnUpldr();

            callback();
        },

        initFnUpldr: function () {
            var i, options, uploadBtn, msgDiv, fileArray, uploaderDom, fileList = this.getFileList();

            //Split comma delimited list of extensions
            fileArray = (fileList === null) ? [] : fileList.split(",");

            //Remove MIME types from array
            for (i = 0; i < fileArray.length; i += 1) {
                if (fileArray[i].indexOf("/") !== -1 || fileArray[i].indexOf(".") === -1) {
                    fileArray.splice(i, 1);
                }
            }

            //Setup Fine Uploader options
            options = {
                debug: this.debugMsgs,
                autoUpload: (this.autoLoad && this.errMethod === "dialog"),
                element: this.domNode,
                maxConnections: this.concurrency,
                request: {
                    endpoint: '/rest/processupload/',
                    params: {
                        entity: this.fileDocEntity,
                        idAttr: this.idAttr,
                        fileSizeAttr: this.fileSizeAttr,
                        contextPath: this.assocContext,
                        contextID: (this.assocContext) ? this._contextObj.getGuid() : "",
                        chunk: false
                    }
                },
                deleteFile: {
                    enabled: true,
                    endpoint: '/rest/deleteupload/',
                    method: 'POST'
                },
                ui: {
                    template: this.domNode.firstElementChild.firstElementChild
                },
                display: {
                    fileSizeOnSubmit: true
                },
                callbacks: {
                    onProgress: dojo.hitch(this, this.onProgress),
                    onSubmitDelete: dojo.hitch(this, this.delFile),
                    onValidate: dojo.hitch(this, this.fileValidation),
                    onUploadChunk: dojo.hitch(this, this.onUploadChunk),
                    onComplete: dojo.hitch(this, this.onComplete),
                    onAllComplete: dojo.hitch(this, this.allCompleteHndlr),
                    onSubmit: dojo.hitch(this, this.onSubmit),
                    onSubmitted: dojo.hitch(this, this.onSubmitted),
                    onCancel: dojo.hitch(this, this.onCancel),
                    onError: dojo.hitch(this, this.onErr)
                },
                validation: {
                    //Turn off integrated validation when not using dialog
                    itemLimit: (this.errMethod !== "dialog") ? 0 : this.maxNoFiles,
                    acceptFiles: (this.errMethod !== "dialog") ? null : fileList,
                    allowedExtensions: (this.errMethod !== "dialog") ? [] : fileArray,
                    stopOnFirstInvalidFile: (this.errMethod === "dialog"),
                    sizeLimit: (this.errMethod !== "dialog") ? 0 : this.maxFileSize * 1024 * 1024
                }
            };

            //Set chunking settings if necessary
            if (this.chunking) {
                options.chunking = {
                    enabled: this.chunking,
                    concurrent: {
                        enabled: this.concurrentChunks
                    },
                    success: {
                        endpoint: "/rest/processupload/"
                    },
                    partSize: this.maxChunkSize
                };
            }

            if (this.errMethod !== "dialog") {
                options.showMessage = dojo.hitch(this, function (msg, name, promise) {
                    //Add name to object for processing of message and status later
                    this.invalidFiles[name] = {msg: msg};

                    //Set promise successful to alert validation that the name is stored
                    promise.success(true);
                });
            }

            //Set error messages
            options.messages = {
                sizeError: (this.fileSzErrMsg !== "") ? this.fileSzErrMsg : "{file} is too large, maximum file size is " + this.maxFileSize + "MB."
            };
            if (this.fileTypeErrMsg !== "") {
                options.messages.typeError = this.fileTypeErrMsg;
            }


            //Initialize Fine Uploader instance
            this.uploader = new qq.FineUploader(options);

            //Create button for triggering manual uploads
            if (!this.autoLoad) {
                this.uploadBtn = new mxui.widget._Button();
                this.uploadBtn.set({
                    caption: this.btnCaption,
                    iconUrl: this.btnImage
                });
                this.uploadBtn.on("click", dojo.hitch(this, this.btnOnClick));
                this.uploadBtn.domNode.style.marginTop = "10px";
                this.uploadBtn.domNode.disabled = true;
                this.domNode.appendChild(this.uploadBtn.domNode);
            }

            //Create button for clearing uploads
            if (this.showClearBtn) {
                this.clearBtn = new mxui.widget._Button();
                this.clearBtn.set({
                    caption: "Clear Uploads"
                });
                this.clearBtn.on("click", dojo.hitch(this, this.clearUploads));
                this.clearBtn.domNode.style.marginTop = "10px";
                this.domNode.appendChild(this.clearBtn.domNode);
            }

            //Add validation message display above drop area
            if (this.validationMsg) {
                msgDiv = document.createElement("DIV");

                msgDiv.style.height = "28px";
                msgDiv.className = "qq-validation-msg";
                msgDiv.innerHTML = "<p>" + this.vMsgText + "</p>";
                $("#" + this.domNode.id).prepend(msgDiv);

                this.msgElement = msgDiv.firstChild;
                this.msgElement.style.display = "none";
            }

            if (this.clickZn) {
                $('#' + this.domNode.id + ' .qq-upload-button').css("display", "none");
                //$('#' + this.domNode.id + ' .qq-upload-list-container').css("height", "100%");

                //uploaderDom = $('#' + this.domNode.id + ' .qq-uploader');
                uploaderDom = $('#' + this.domNode.id + ' .qq-upload-list-container');
                uploaderDom.css("cursor", "pointer");
                uploaderDom.click(dojo.hitch(this, function (e) {
                    //Stop if not clicking in right area or files are being uploaded
                    if (e.target !== $('#' + this.domNode.id + ' .qq-upload-list-container')[0]) {
                        e.bubbles = false;
                        e.stopPropagation();
                        return;
                    } else if (this.hasUploadStarted()) {
                        return;
                    }

                    //Show dialog window
                    $('#' + this.domNode.id + ' input[name="qqfile"]')[0].click();
                }));
            } else {
                $('#' + this.domNode.id + ' .qq-upload-list-container').css("height", "78%");
            }

            if (!this.overallProgress) {
                $('#' + this.domNode.id + ' .qq-total-progress-bar-container').css("display", "none");
            }
        },

        setupStyles: function () {
            var i,
                styleSheets = document.styleSheets,
                widgetSheet,
                rules,
                ruleFound = false;

            if (this.dropCapOrImg === "dropImg") {
                //Find widgets.css stylesheet
                for (i = 0; i < styleSheets.length; i += 1) {
                    if (styleSheets[i].href.indexOf("widgets.css") > -1) {
                        widgetSheet = styleSheets[i];
                        break;
                    }
                }

                //Search rules for .qq-uploader-img
                rules = widgetSheet.cssRules;
                for (i in rules) {
                    if (rules.hasOwnProperty(i) && i !== "length") {
                        if (rules[i].selectorText.indexOf(".qq-uploader-img") > -1) {
                            ruleFound = true;
                            break;
                        }
                    }
                }

                //Add rule if not found
                if (!ruleFound) {
                    widgetSheet.insertRule(".qq-uploader-img:before {content: url(../" + this.dropImage + ") }", widgetSheet.cssRules.length);
                }
            }
        },

        alterTemplate: function () {
            var widgetId = '#' + this.domNode.id,
                filesLI,
                delBtn,
                editIcon,
                editInput,
                cancelBtn,
                retryBtn,
                template = this.qqTemplate.innerHTML,
                tempDom = document.createElement('DIV');

            tempDom.style.display = "none";
            tempDom.innerHTML = template;
            this.domNode.appendChild(tempDom);

            if (this.dropCapOrImg === "dropCap") {
                $(widgetId + " .qq-uploader")[0].setAttribute("qq-drop-area-text", this.dropCaption);
            } else {
                $(widgetId + " .qq-uploader").addClass("qq-uploader-img");
            }

            $(widgetId + " .qq-uploader")[0].setAttribute("qq-drop-area-text", (this.dropCapOrImg === "dropCap") ? this.dropCaption : (this.dropImage !== "") ? this.dropImage : this.dropCaption);

            filesLI = $(widgetId + ' li')[0];

            if (!this.allowDelete) {
                delBtn = $(widgetId + ' li .qq-upload-delete')[0];
                filesLI.removeChild(delBtn);
            }

            if (!this.allowNameEdit) {
                editIcon = $(widgetId + ' li .qq-edit-filename-icon')[0];
                editInput = $(widgetId + ' li .qq-edit-filename')[0];
                filesLI.removeChild(editIcon);
                filesLI.removeChild(editInput);
            }

            if (!this.allowCancel) {
                cancelBtn = $(widgetId + ' li .qq-upload-cancel')[0];
                filesLI.removeChild(cancelBtn);
            }

            if (!this.allowRetry) {
                retryBtn = $(widgetId + ' li .qq-upload-retry')[0];
                filesLI.removeChild(retryBtn);
            }

            this.qqTemplate.innerHTML = $(widgetId + ' > div')[0].innerHTML;
        },

        delFile: function(id, name) {
            var file, params = {
                qquuid: id,
                fileEntity: this.fileDocEntity,
                idAttr: this.idAttr
            };

            file = this.uploader.getFile(id);
            params.autoid = file.autoid.replace(this.fileDocEntity, "");

            this.uploader.setDeleteFileParams(params, id);
        },

        getFileList: function () {
            var type, commaFileList = null;

            if (this.fileTypeList && this.fileTypeList.length > 0) {
                for (type in this.fileTypeList) {
                    if (typeof type === "string") {
                        commaFileList = (commaFileList === null) ? this.fileTypeList[type].type : commaFileList + "," + this.fileTypeList[type].type;
                    }
                }

                commaFileList = commaFileList.replace(/\./g, "");
            }

            return commaFileList;
        },

        btnOnClick: function () {
            var me = this, guid, args, startUpload = function () {
                if (me.hideBtn) {
                    me.uploadBtn.domNode.style.display = "none";
                } else {
                    me.uploadBtn.domNode.disabled = true;
                }
                me.uploader.uploadStoredFiles();
            };

            if (me.uploadBtnMf !== "") {
                guid = me._contextObj.getGuid();
                args = {
                    params: {
                        actionname: me.uploadBtnMf,
                        applyto: "selection",
                        guids: [guid]
                    },
                    callback: dojo.hitch(me, function (value) {
                        if (value) {
                            startUpload();
                        }
                    })
                };

                mx.data.action(args);
            } else {
                startUpload();
            }
        },

        clearUploads: function () {
            var me = this,
                guid,
                args,
                i,
                files = me.uploader.getUploads(),
                file,
                clear = function () {
                    for (i = 0; i < files.length; i += 1) {
                        //Switch status to canceled if necessary
                        switch (files[i].status) {
                            case qq.status.UPLOAD_FAILED:
                            case qq.status.UPLOAD_SUCCESSFUL:
                            case qq.status.CANCELED:
                            //case qq.status.REJECTED:  This status must be changed to stop validation of the file after clearing
                            case qq.status.DELETED:
                                break;
                            default:
                                me.uploader._uploadData.setStatus(files[i].id, qq.status.CANCELED);
                                me.uploader._storedIds.shift();
                        }
                    }

                    //Remove files from list
                    $("#" + me.domNode.id + " .qq-upload-list").empty();

                    //Disable upload button
                    if (!me.autoLoad) {
                        me.uploadBtn.domNode.disabled = true;
                    }
                };

            if (me.clearBtnMf !== "") {
                guid = me._contextObj.getGuid();
                args = {
                    params: {
                        actionname: me.clearBtnMf,
                        applyto: "selection",
                        guids: [guid]
                    },
                    callback: dojo.hitch(me, function (value) {
                        if (value) {
                            clear();
                            if (me.validationMsg) {me.msgElement.style.display = "none";}
                            me.invalidFiles = {};
                        }
                    })
                };

                mx.data.action(args);
            } else {
                clear();
                if (me.validationMsg) {me.msgElement.style.display = "none";}
                me.invalidFiles = {};
            }
        },

        allCompleteHndlr: function () {
            var me = this, guid, args, mfPromise;

            mfPromise = new qq.Promise();
            mfPromise.done(function () {
                if (!me.autoLoad) {
                    if (me.hideBtn) {
                        me.uploadBtn.domNode.style.display = "inline-block";
                    }

                    me.uploadBtn.domNode.disabled = !me.checkQueued();
                }
            });

            if (this.allCompleteMf !== "") {
                guid = me._contextObj.getGuid();
                args = {
                    params: {
                        actionname: me.allCompleteMf,
                        applyto: "selection",
                        guids: [guid]
                    },
                    callback: dojo.hitch(me, function () {
                        mfPromise.success();
                    }),
                    error: dojo.hitch(me, function () {
                        mfPromise.failure();
                    })
                };

                mx.data.action(args);
            }
        },

        fileValidation: function (data) {
            var i, found = false, extension, fileList, fileArray, isValid = true, fileCnt, errMsg = "", msgPromise = null;

            fileCnt = this.uploader.getUploads({
                status: qq.status.SUBMITTED
            }).length;

            if (this.errMethod !== "dialog") {
                //File Size
                if (this.maxFileSize > 0 && data.size > this.maxFileSize * 1024 * 1024) {
                    errMsg = (this.fileSzErrMsg !== "") ? this.fileSzErrMsg : data.name + " is too large, maximum file size is " + this.maxFileSize + "MB.";

                    msgPromise = new qq.Promise();

                    this.uploader._options.showMessage(errMsg, data.name, msgPromise);
                } else

                //Number of File
                if (this.maxNoFiles > 0 && fileCnt >= this.maxNoFiles) {
                    errMsg = "You cannot upload any more files.";

                    msgPromise = new qq.Promise();

                    this.uploader._options.showMessage(errMsg, data.name, msgPromise);
                } else

                //File Type
                if (this.fileTypeList !== null) {
                    extension = data.name.substr(data.name.lastIndexOf("."));
                    extension = extension.replace(".", "").toLowerCase();

                    fileList = this.getFileList();

                    //Split comma delimited list of extensions
                    fileArray = fileList.split(",");

                    //Remove MIME types from array
                    for (i = 0; i < fileArray.length; i += 1) {
                        if (fileArray[i].indexOf("/") === -1) {
                            found = (fileArray[i] === extension);

                            if (found) {break;}
                        }
                    }

                    if (!found) {
                        errMsg = (this.fileTypeErrMsg !== "") ? this.fileTypeErrMsg : data.name + " has an invalid extension. Valid extension(s): " + fileList + ".";

                        msgPromise = new qq.Promise();

                        this.uploader._options.showMessage(errMsg, data.name, msgPromise);
                    }
                }

                if (!this.autoLoad && (this.checkRejected() || errMsg !== "") && !this.hasUploadStarted()) {
                    this.uploadBtn.domNode.disabled = true;
                    if (this.validationMsg) {this.msgElement.style.display = "block";}
                }

                if (errMsg !== "") {
                    return msgPromise;
                }
            }

            //File Dups
            if (!this.allowDups && !this.checkDups(data)) {
                isValid = (this.errMethod !== "dialog");

                if (this.fileDupErrMsg !== "") {
                    errMsg = this.fileDupErrMsg;
                } else {
                    errMsg = data.name + " is a duplicate file name.";
                }

                if (this.errMethod !== "dialog") {
                    msgPromise = new qq.Promise();
                    this.uploader._options.showMessage(errMsg, data.name, msgPromise);
                } else {
                    this.uploader._options.showMessage(errMsg);
                }
            }

            if (!this.autoLoad && (this.checkRejected() || errMsg !== "") && !this.hasUploadStarted()) {
                this.uploadBtn.domNode.disabled = true;
                if (this.validationMsg) {this.msgElement.style.display = "block";}
            } else if (!this.autoLoad) {
                this.uploadBtn.domNode.disabled = false;
                if (this.validationMsg) {this.msgElement.style.display = "none";}
            }

            return (msgPromise !== null) ? msgPromise : isValid;
        },

        replaceFileSize: function (sizeElem) {
            var fileSizeStr, fileSize, rndFunc;

            rndFunc = function (value, decimals) {
                return Number(Math.round(value +'e'+ decimals) +'e-'+ decimals).toFixed(decimals);
            };

            fileSizeStr = sizeElem[0].innerHTML;

            if (fileSizeStr.indexOf("kB") > -1) {
                fileSize = fileSizeStr.replace("kB", "");
                fileSize = (fileSize * Math.pow(1000, 1)) / Math.pow(1024, 1);
                fileSizeStr = rndFunc(fileSize, 1) + " kB";
            } else if (fileSizeStr.indexOf("MB") > -1) {
                fileSize = fileSizeStr.replace("MB", "");
                fileSize = (fileSize * Math.pow(1000, 2)) / Math.pow(1024, 2);
                fileSizeStr = rndFunc(fileSize, 1) + " MB";
            } else if (fileSizeStr.indexOf("GB") > -1) {
                fileSize = fileSizeStr.replace("GB", "");
                fileSize = (fileSize * Math.pow(1000, 3)) / Math.pow(1024, 3);
                fileSizeStr = rndFunc(fileSize, 1) + " GB";
            } else if (fileSizeStr.indexOf("TB") > -1) {
                fileSize = fileSizeStr.replace("TB", "");
                fileSize = (fileSize * Math.pow(1000, 4)) / Math.pow(1024, 4);
                fileSizeStr = rndFunc(fileSize, 1) + " TB";
            } else {
                return;
            }

            sizeElem[0].innerHTML = fileSizeStr;
        },

        onProgress: function (id, name, uploadedBytes, totalBytes) {
            var origFileSize, fileSize, sizeElem, fileSizeStr, rndFunc;

            rndFunc = function (value, decimals) {
                return Number(Math.round(value +'e'+ decimals) +'e-'+ decimals).toFixed(decimals);
            };

            //Get li element
            sizeElem = $('#' + this.domNode.id + " li[qq-file-id='" + id + "'] .qq-upload-size");

            //Get file size
            origFileSize = (sizeElem[0].innerHTML.indexOf("of") > -1) ? sizeElem[0].innerHTML.split(" ")[2] : sizeElem[0].innerHTML;

            fileSizeStr = sizeElem[0].innerHTML;

            //Format extension
            if (origFileSize.indexOf("kB") > -1) {
                fileSize = origFileSize.replace("kB", "");
                fileSize = (fileSize * Math.pow(1000, 1)) / Math.pow(1024, 1);
                fileSizeStr = fileSizeStr.replace(origFileSize, rndFunc(fileSize, 1) + " kB");
            } else if (origFileSize.indexOf("MB") > -1) {
                fileSize = origFileSize.replace("MB", "");
                fileSize = (fileSize * Math.pow(1000, 2)) / Math.pow(1024, 2);
                fileSizeStr = fileSizeStr.replace(origFileSize, rndFunc(fileSize, 1) + " MB");
            } else if (origFileSize.indexOf("GB") > -1) {
                fileSize = origFileSize.replace("GB", "");
                fileSize = (fileSize * Math.pow(1000, 3)) / Math.pow(1024, 3);
                fileSizeStr = fileSizeStr.replace(origFileSize, rndFunc(fileSize, 1) + " GB");
            } else if (origFileSize.indexOf("TB") > -1) {
                fileSize = origFileSize.replace("TB", "");
                fileSize = (fileSize * Math.pow(1000, 4)) / Math.pow(1024, 4);
                fileSizeStr = fileSizeStr.replace(origFileSize, rndFunc(fileSize, 1) + " TB");
            } else {
                return;
            }

            sizeElem[0].innerHTML = fileSizeStr;

            if (this.keepAlive) {
                $("html").click();
            }
        },

        onUploadChunk: function (id, name, data) {
            //Change endpoint for processing chunks
            var currEndpoint = this.uploader.getEndpoint(id);

            if (currEndpoint !== "/rest/processchunks/") {
                this.uploader.setEndpoint("/rest/processchunks/", id);
                this.uploader.setParams({
                    chunk: true,
                    entity: this.fileDocEntity,
                    idAttr: this.idAttr,
                    fileSizeAttr: this.fileSizeAttr,
                    contextPath: this.assocContext,
                    contextID: (this.assocContext) ? this._contextObj.getGuid() : ""
                }, id);
            }
        },

        onComplete: function (id, name, response) {
            var file, sizeElem, fileSize, fileSizeStr;

            if (!!response.returnedId) {
                file = this.uploader.getFile(id);
                file.autoid = this.fileDocEntity + response.returnedId;
            }

            //Get li element
            sizeElem = $('#' + this.domNode.id + " li[qq-file-id='" + id + "'] .qq-upload-size");

            if (!this.showFileSize) {
                //Add hide class
                sizeElem.removeClass("qq-hide");
            }


        },

        onCancel: function (id) {
            if (!this.autoLoad && this.checkRejected(id)) {
                this.uploadBtn.domNode.disabled = true;
                if (this.validationMsg) {this.msgElement.style.display = "block";}
            } else if (!this.autoLoad) {
                this.uploadBtn.domNode.disabled = !this.checkQueued(id);
                if (this.validationMsg) {this.msgElement.style.display = "none";}
            }
        },

        onErr: function (id, name, errMsg) {
            var onErrPromise, obj, createArgs, mfArgs, createSucess, createFail, me = this;

            if (me.onErrMf !== "") {
                //Setup promise
                onErrPromise = new qq.Promise();

                //Handler if successful object creation
                createSucess = function (obj) {
                    //Populate attributes of entity including errAttr if supplied
                    obj.set("Name", name);
                    if (me.fileSizeAttr !== "") {
                        obj.set(me.fileSizeAttr, me.uploader.getFile(id).size);
                    }
                    if (me.onErrAttr !== "") {
                        obj.set(me.onErrAttr, errMsg);
                    }

                    //Setup arguments for running microflow
                    mfArgs = {
                        params: {
                            actionname: me.onErrMf,
                            applyto: "selection",
                            guids: [obj.getGuid()]
                        },
                        callback: function () {
                            if (me.debugMsgs) {
                                console.log("On Error Microflow successfully executed");
                            }
                        },
                        error: function (err) {
                            console.log("On Error Microflow failed to execute.\n\n" + err);
                        }
                    };

                    //Run microflow with object supplied
                    mx.data.action(mfArgs);
                };

                //Handler if object creation fails
                createFail = function (e) {
                    console.log("Unable to run On Error microflow.  Object could not be created.\n\n" + e);
                };

                //Add handlers to promise
                onErrPromise.then(createSucess, createFail);

                createArgs = {
                    entity: me.fileDocEntity,
                    callback: function (obj) {
                        onErrPromise.success(obj);
                    },
                    error: function (e) {
                        onErrPromise.failure(e);
                    }
                };

                //Create object
                mx.data.create(createArgs);
            }
        },

        onSubmit: function (id, name) {
            //Reject submitted file if in the process of uploading files
            if (this.hasUploadStarted()) {
                return false;
            }
        },

        onSubmitted: function (id, name) {
            var file, sizeElem, fileSizeStr, fileSize, rndFunc, processInvalid = dojo.hitch(this, this.processInvalid), fileNodeObserver;

            processInvalid(id, name);

            file = this.uploader.getUploads({
              id: id
            });

            if (this.errMethod !== "inline" && file.status === qq.status.REJECTED) {
                return;
            }

            //Get li element
            sizeElem = $('#' + this.domNode.id + " li[qq-file-id='" + id + "'] .qq-upload-size");

            if (!this.showFileSize) {
                //Add hide class
                sizeElem.addClass("qq-hide");
            } else {
                this.replaceFileSize(sizeElem);
            }
        },

        processInvalid: function (id, name) {
            var invalidObj, fileObj, statusDOM;

            if (this.errMethod !== "dialog") {
                //Check if file name is in object of invalid files
                invalidObj = this.invalidFiles[name];

                if (invalidObj === undefined) {
                    if (this.autoLoad) {
                        this.uploader.retry(id);
                        return;
                    }
                } else {
                    //Set file's status message
                    $('#' + this.domNode.id + ' .qq-file-id-' + id).addClass("qq-errored-file");
                    $('#' + this.domNode.id + ' .qq-file-id-' + id + ' .qq-upload-status-text')[0].innerHTML = invalidObj.msg;

                    //Change status to qq.status.REJECTED
                    this.uploader._uploadData.setStatus(id, qq.status.REJECTED);

                    //Remove name from object
                    delete this.invalidFiles[name];
                }
            }
        },

        checkQueued: function (id) {
            var i, uploadCnt, uploads = this.uploader.getUploads({
                status: qq.status.SUBMITTED
            });

            //Exclude id from list of uploads if provided
            if (id !== undefined) {
                for (i = 0; i < uploads.length; i += 1) {
                    if (id === uploads[i].id) {
                        uploads.splice(i, 1);
                        break;
                    }
                }
            }

            uploadCnt = uploads.length;

            return (uploadCnt > 0);
        },

        checkDups: function (file) {
            var i,
                fileNameArray = [],
                uploadsArray = this.uploader.getUploads({
                    status: qq.status.SUBMITTED
                });

            if (uploadsArray.length > 0) {
                //Get list of upload names
                for (i = 0; i < uploadsArray.length; i += 1) {
                    fileNameArray.push(uploadsArray[i].name);
                }

                //Look for file name in list of upload names
                return (fileNameArray.indexOf(file.name) > -1) ? false : true;
            } else {
                return true;
            }
        },

        checkRejected: function (id) {
            var i, currentFile, rejectCnt, rejectFiles = this.uploader.getUploads({
                status: qq.status.REJECTED
            });

            if (id !== undefined) {
                currentFile = this.uploader.getFile(id);

                for (i = 0; i < rejectFiles.length; i += 1) {
                    if (currentFile === this.uploader.getFile(rejectFiles[i].id)) {
                        rejectFiles.splice(i, 1);
                        break;
                    }
                }
            }

            rejectCnt = rejectFiles.length;

            return (rejectCnt > 0);
        },

        hasUploadStarted: function () {
            var returnVal = false, uploads;

            /*Get list of files that have been uploaded or are
              in the process of being uploaded*/
            uploads = this.uploader.getUploads({
              status: [/*qq.status.SUBMITTED, */qq.status.QUEUED, qq.status.UPLOADING, qq.status.UPLOAD_RETRYING, qq.status.UPLOAD_FAILED, qq.status.UPLOAD_SUCCESSFUL, qq.status.PAUSED]
            });

            //Reject submitted file if in the process of uploading files
            if (uploads.length > 0) {
                returnVal = true;
            }

            return returnVal;
        },

        printDebugMsg: function (msg) {
            if (this.debugMsgs) {
                console.log(msg);
            }
        },

        // mxui.widget._WidgetBase.enable is called when the widget should enable editing. Implement to enable editing if widget is input widget.
        enable: function() {
          logger.debug(this.id + ".enable");
        },

        // mxui.widget._WidgetBase.enable is called when the widget should disable editing. Implement to disable editing if widget is input widget.
        disable: function() {
          logger.debug(this.id + ".disable");
        },

        // mxui.widget._WidgetBase.resize is called when the page's layout is recalculated. Implement to do sizing calculations. Prefer using CSS instead.
        resize: function(box) {
          logger.debug(this.id + ".resize");
        },

        // mxui.widget._WidgetBase.uninitialize is called when the widget is destroyed. Implement to do special tear-down work.
        uninitialize: function() {
          logger.debug(this.id + ".uninitialize");
            // Clean up listeners, helper objects, etc. There is no need to remove listeners added with this.connect / this.subscribe / this.own.
        }
    };

    // Declare widget's prototype.
    mxui.widget.declare("FineUploader.widget.FineUploader", widget);
});
